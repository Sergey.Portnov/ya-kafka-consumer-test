#!/usr/bin/env sh

JAVA_OPTS="${JAVA_OPTS} -XX:+UseContainerSupport -XX:InitialRAMPercentage=20 -XX:MaxRAMPercentage=50"

if [ "${JAVA_DEBUG_ENABLED}" = "true" ]; then
    JAVA_OPTS="${JAVA_OPTS} -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005"
fi

exec java ${JAVA_OPTS} -jar /app/${PACKAGE_NAME}.jar

FROM bellsoft/liberica-openjdk-alpine:17

COPY .entrypoint.sh /tmp/
RUN tr -d '\r' < /tmp/.entrypoint.sh > /entrypoint.sh && chmod 0755 /entrypoint.sh
COPY target/ya-kafka-consumer-test.jar /app/
ENV PACKAGE_NAME ya-kafka-consumer-test

RUN mkdir -p /usr/local/share/ca-certificates/Yandex && \
    wget "https://storage.yandexcloud.net/cloud-certs/CA.pem" -O /usr/local/share/ca-certificates/Yandex/YandexCA.crt

RUN keytool -keystore client.truststore.jks -noprompt -alias CARoot -import -file /usr/local/share/ca-certificates/Yandex/YandexCA.crt -storepass changeit
RUN mkdir -p /etc/security/ssl && cp client.truststore.jks /etc/security/ssl

ENTRYPOINT ["/entrypoint.sh"]

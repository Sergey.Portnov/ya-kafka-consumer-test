package com.gitalb.sergey.portnov.kafka.test.config.properties;

import javax.validation.constraints.NotEmpty;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;
import org.springframework.validation.annotation.Validated;

@Validated
@Data
public class KafkaTrustStoreProperties {

  @NotEmpty
  private String path;
  @NotEmpty
  private String password;

  public boolean isEmpty() {
    return StringUtils.isAllBlank(path, password);
  }
}

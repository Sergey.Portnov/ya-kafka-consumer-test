package com.gitalb.sergey.portnov.kafka.test.consumer;

import com.gitalb.sergey.portnov.kafka.test.model.TestModel;
import java.util.concurrent.TimeUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class TestConsumer {

  @KafkaListener(topics = "${kafka.consumer.test.topic}", containerFactory = "testListenerContainerFactory")
  public void consumePayment(TestModel model, Acknowledgment acknowledgment) {
    var stopTime =  System.nanoTime() + TimeUnit.MILLISECONDS.toNanos(50);
    while (stopTime > System.nanoTime()) {
    }
    log.info("Received {}", model);
    acknowledgment.acknowledge();
  }
}

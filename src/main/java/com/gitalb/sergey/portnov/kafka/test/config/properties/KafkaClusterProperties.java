package com.gitalb.sergey.portnov.kafka.test.config.properties;

import java.util.List;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import lombok.Data;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.validation.annotation.Validated;

@Validated
@Data
public class KafkaClusterProperties {

  @NotEmpty
  private List<String> bootstrapServers;
  @Valid
  @NestedConfigurationProperty
  private KafkaSecurityProperties security;
}

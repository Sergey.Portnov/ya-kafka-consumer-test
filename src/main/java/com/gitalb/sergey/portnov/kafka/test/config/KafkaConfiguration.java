package com.gitalb.sergey.portnov.kafka.test.config;

import com.gitalb.sergey.portnov.kafka.test.config.properties.KafkaClusterProperties;
import com.gitalb.sergey.portnov.kafka.test.config.properties.KafkaConsumerProperties;
import com.gitalb.sergey.portnov.kafka.test.config.properties.KafkaSecurityProperties;
import com.gitalb.sergey.portnov.kafka.test.config.properties.KafkaTrustStoreProperties;
import com.gitalb.sergey.portnov.kafka.test.model.TestModel;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.kafka.clients.CommonClientConfigs;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.config.SaslConfigs;
import org.apache.kafka.common.config.SslConfigs;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.listener.ContainerProperties;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;

@Configuration
public class KafkaConfiguration {

  @Bean
  @ConfigurationProperties("kafka.consumer.test")
  public KafkaConsumerProperties testsConsumerProperties() {
    return new KafkaConsumerProperties();
  }

  @Bean
  public ConcurrentKafkaListenerContainerFactory<String, TestModel> testListenerContainerFactory(
      KafkaConsumerProperties testsConsumerProperties) {
    return createKafkaListenerContainerFactory(TestModel.class, testsConsumerProperties);
  }

  private <T> ConcurrentKafkaListenerContainerFactory<String, T> createKafkaListenerContainerFactory(
      Class<T> messageType,
      KafkaConsumerProperties consumerProperties) {
    var containerFactory = new ConcurrentKafkaListenerContainerFactory<String, T>();
    var valueDeserializer = new ErrorHandlingDeserializer<>(new JsonDeserializer<>(messageType, false));

    containerFactory.setConsumerFactory(
        new DefaultKafkaConsumerFactory<>(
            buildConsumerConfigs(consumerProperties),
            new StringDeserializer(),
            valueDeserializer
        ));

    containerFactory.getContainerProperties().setAckMode(ContainerProperties.AckMode.MANUAL);

    return containerFactory;
  }

  private Map<String, Object> buildConsumerConfigs(KafkaConsumerProperties consumerProperties) {

    KafkaClusterProperties clusterProperties = consumerProperties.getCluster();

    Map<String, Object> clusterConfigs = Map.of(
        ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, clusterProperties.getBootstrapServers(),
        ConsumerConfig.GROUP_ID_CONFIG, consumerProperties.getConsumerGroupId(),
        ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest",
        ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false"
    );
    Map<String, Object> securityConfigs = buildSecurityConfigs(clusterProperties);
    Map<String, Object> trustStoreConfigs = buildTrustStoreConfigs(clusterProperties);

    return Stream.of(clusterConfigs, securityConfigs, trustStoreConfigs)
        .flatMap(m -> m.entrySet().stream())
        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
  }

  private Map<String, Object> buildSecurityConfigs(KafkaClusterProperties clusterProperties) {
    if (clusterProperties.getSecurity() == null || clusterProperties.getSecurity().isEmpty()) {
      return Map.of();
    }

    var securityProperties = clusterProperties.getSecurity();
    var jaasConfigString = String.format(securityProperties.getSaslJaasConfigStringTemplate(),
        securityProperties.getUsername(), securityProperties.getPassword()
    );
    return Map.of(
        CommonClientConfigs.SECURITY_PROTOCOL_CONFIG, securityProperties.getSecurityProtocol(),
        SaslConfigs.SASL_MECHANISM, securityProperties.getSaslMechanism(),
        SaslConfigs.SASL_JAAS_CONFIG, jaasConfigString
    );
  }

  private Map<String, Object> buildTrustStoreConfigs(KafkaClusterProperties clusterProperties) {
    boolean noTrustStoreProperties = Optional.ofNullable(clusterProperties.getSecurity())
        .map(KafkaSecurityProperties::getTrustStore)
        .map(KafkaTrustStoreProperties::isEmpty)
        .orElse(true);

    if (noTrustStoreProperties) {
      return Map.of();
    }
    var trustStoreProperties = clusterProperties.getSecurity().getTrustStore();

    return Map.of(
        SslConfigs.SSL_TRUSTSTORE_LOCATION_CONFIG, trustStoreProperties.getPath(),
        SslConfigs.SSL_TRUSTSTORE_PASSWORD_CONFIG, trustStoreProperties.getPassword()
    );
  }
}

package com.gitalb.sergey.portnov.kafka.test.model;

import java.math.BigDecimal;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class TestModel {

  String stringField;
  BigDecimal bigDecimalField;
  TestEnum enumField;
  NestedModel nestedModelField;

  public enum TestEnum {
    A,
    B
  }

  @Value
  @Builder
  public static class NestedModel {
    String stringField;
  }
}

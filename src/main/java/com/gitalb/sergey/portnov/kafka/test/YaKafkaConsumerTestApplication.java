package com.gitalb.sergey.portnov.kafka.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class YaKafkaConsumerTestApplication {

  public static void main(String[] args) {
    SpringApplication.run(YaKafkaConsumerTestApplication.class, args);
  }

}
